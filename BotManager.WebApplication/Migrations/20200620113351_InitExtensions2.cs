﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BotManager.WebApplication.Migrations
{
    public partial class InitExtensions2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Options",
                table: "Extensions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Options",
                table: "Extensions");
        }
    }
}
