﻿// <auto-generated />
using System;
using ExtCore.Data.EntityFramework.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BotManager.WebApplication.Migrations
{
    [DbContext(typeof(StorageContext))]
    [Migration("20200620113351_InitExtensions2")]
    partial class InitExtensions2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0");

            modelBuilder.Entity("BotManager.Entities.Bot", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<int?>("TokenId")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.HasIndex("TokenId");

                    b.ToTable("Bots");
                });

            modelBuilder.Entity("BotManager.Entities.Extension", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int?>("BotId")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("IsEnabled")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Options")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.HasIndex("BotId");

                    b.ToTable("Extensions");
                });

            modelBuilder.Entity("BotManager.Entities.Token", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("BotExternalId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("BotToken")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Tokens");
                });

            modelBuilder.Entity("BotManager.Entities.Bot", b =>
                {
                    b.HasOne("BotManager.Entities.Token", "Token")
                        .WithMany()
                        .HasForeignKey("TokenId");
                });

            modelBuilder.Entity("BotManager.Entities.Extension", b =>
                {
                    b.HasOne("BotManager.Entities.Bot", "Bot")
                        .WithMany()
                        .HasForeignKey("BotId");
                });
#pragma warning restore 612, 618
        }
    }
}
