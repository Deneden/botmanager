using System.Reflection;
using BotManager.Abstractions;
using BotManager.Entities;
using BotManager.WebApplication.Services;
using ExtCore.Data.EntityFramework;
using ExtCore.WebApplication.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BotManager.WebApplication
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly string _extensionsPath;

        public Startup(IHostEnvironment hostingEnvironment, IConfiguration configuration)
        {
            _configuration = configuration;
            _extensionsPath = hostingEnvironment.ContentRootPath + configuration["Extensions:Path"];
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IBotService, BotService>();
            services.AddScoped<IExtensionsService, ExtensionsService>();
            services.Configure<AppConfig>(_configuration.GetSection("AppConfiguration"));
            services.AddExtCore(_extensionsPath);
            services.AddControllers().AddNewtonsoftJson();
            services.Configure<StorageContextOptions>(
                options =>
                {
                    options.ConnectionString = _configuration.GetConnectionString("Default");
                    options.MigrationsAssembly = typeof(DesignTimeStorageContextFactory).GetTypeInfo().Assembly.FullName;
                }
            );

            DesignTimeStorageContextFactory.Initialize(services.BuildServiceProvider());
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExtCore();
        }
    }
}