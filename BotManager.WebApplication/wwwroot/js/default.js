﻿function setTab(tabConnentClass, tabLinksClass) {
    let items = localStorage.getItem(tabConnentClass).split(',');
    let activeTab = items[0];
    let activeLink = items[1];
    openTab(activeLink, activeTab, tabConnentClass, tabLinksClass, true);
};
function saveTab(tabLink, tabName, tabConnentClass) {
    localStorage.setItem(tabConnentClass, [ tabName, tabLink ]);
};
function openTab(tabLink, tabName, tabConnentClass, tabLinksClass, resetTabs) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName(tabConnentClass);
    var element = document.getElementById(tabName);
    if (element == null && !resetTabs)
        return;
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName(tabLinksClass);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    var link = document.getElementById(tabLink);
    if (element == null && resetTabs) {
        element = tabcontent[0];
        link = tablinks[0];
    }
    if (element == null)
        return;
    element.style.display = "block";
    if (link != null)
        link.className += " active";
}
function sendData(url, formName) {
    formElement = document.getElementById(formName);
    let formData = new FormData(formElement);
    // Fixing the way FormData works with checkboxes (https://stackoverflow.com/questions/33487360/formdata-and-checkboxes)
    for (let i = 0; i < formElement.elements.length; i++) {
        if (formElement.elements[i].type === "checkbox") {
            formData.delete(formElement.elements[i].name);
            formData.append(formElement.elements[i].name, formElement.elements[i].checked);
        }
    }
    let data = new URLSearchParams(formData);
    return fetch(url, { method: "POST", body: data });
}
function confirmAction(message, callback) {
    result = confirm(message);
    if (result)
        callback();
}
function removeElement(elementId) {
    document.getElementById(elementId).remove();
}
function changeText(elementId, text) {
    document.getElementById(elementId).innerText = text;
}
function changeColor(elementId, color) {
    document.getElementById(elementId).style.backgroundColor = color;
}
function deleteBot(url, formName, contentName, buttonName) {
    sendData(url, formName);
    removeElement(contentName);
    changeText(buttonName, '[Удалено]');
    changeColor(buttonName, "#ff5959");
    document.getElementById(buttonName).onclick = null;
    openTab(null, null, 'tabcontent', 'tablinks', true);
}