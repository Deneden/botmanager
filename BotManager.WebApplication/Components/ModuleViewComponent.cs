﻿using BotManager.WebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BotManager.WebApplication.Components
{
    public class ModuleViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(int botId, List<(string, bool)> data)
        {
            // ReSharper disable once Mvc.ViewComponentViewNotResolved
            return View(new ModuleModel()
            {
                Data = data,
                BotId = botId
            });
        }
    }
}
