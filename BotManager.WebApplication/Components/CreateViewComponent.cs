﻿using Microsoft.AspNetCore.Mvc;

namespace BotManager.WebApplication.Components
{
    public class CreateViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            // ReSharper disable once Mvc.ViewComponentViewNotResolved
            return View();
        }
    }
}
