using System;
using BotManager.Abstractions;
using BotManager.Entities;
using BotManager.WebApplication.Models;
using ExtCore.Data.Abstractions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BotManager.WebApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly IStorage _storage;
        private readonly IBotService _botService;
        private readonly IExtensionsService _extensionsService;

        public HomeController(IStorage storage, IBotService botService, IExtensionsService extensionsService)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
            _botService = botService ?? throw new ArgumentNullException(nameof(botService));
            _extensionsService = extensionsService ?? throw new ArgumentNullException(nameof(extensionsService));
        }

        public async Task<IActionResult> Index()
        {
            var model = new IndexModel();

            var bots = await _storage.GetRepository<IBotRepository>().GetAll().ToArrayAsync();
            var updateServices = _extensionsService.GetAllExtensions();

            var  botsExtensions = _storage.GetRepository<IExtensionRepository>().GetAll()
                .ToArray()
                .GroupBy(ex => ex.Bot.Id)
                .ToDictionary(g => g.Key, g =>
                    g.ToDictionary(ex => ex.Name, ex => ex));
            
            foreach (var bot in bots)
            {
                model.BotsWithExtensions.Add(bot, new List<bool>());
                model.Settings.Add(bot.Id, new List<JObject>());
                
                foreach (var service in updateServices)
                {
                    if (botsExtensions.ContainsKey(bot.Id) && botsExtensions[bot.Id].ContainsKey(service.Name))
                    {
                        model.BotsWithExtensions[bot].Add(botsExtensions[bot.Id][service.Name].IsEnabled);
                        model.Settings[bot.Id].Add(botsExtensions[bot.Id][service.Name].Options);
                    }
                    else
                    {
                        model.BotsWithExtensions[bot].Add(false);
                        model.Settings[bot.Id].Add(null);
                    }
                }
            }

            model.ExtensionNames = updateServices
                .Select(ex => ex.Name)
                .ToList();
            
            model.ComponentNames = updateServices
                .Select(ex => ex.ComponentName)
                .ToList();
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(string name, string token)
        {
            if (string.IsNullOrEmpty(name) || name.Length < 3 || name.Length > 100)
                return BadRequest("Name length mus be between 3 and 100");
            
            if (!Token.TryParse(token, out var parsedToken))
                return BadRequest("Can't parse token");
            
            var foundedToken = await _storage.GetRepository<ITokenRepository>()
                .GetAsync(parsedToken.BotExternalId, parsedToken.BotToken);

            if (foundedToken != null)
                parsedToken = foundedToken;

            var botWithToken = await _storage.GetRepository<IBotRepository>()
                .GetAsync(foundedToken);
            
            if(botWithToken != null) return BadRequest("Bot with this token already registered");
            
            var bot = new Bot()
            {
                Name = name,
                Token = parsedToken
            };
            
            bot.IsEnabled = await _botService.TrySetWebHook(bot);
            
            await _storage.GetRepository<IBotRepository>().SaveAsync(bot);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> RemoveAsync(int id)
        {
            var bot = await _storage.GetRepository<IBotRepository>().GetAsync(id);
            
            var result =  await _botService.TryRemoveWebHook(bot);

            if (!result) return BadRequest("Can't remove web hook");
            
            await _storage.GetRepository<IBotRepository>().RemoveAsync(id);

            return RedirectToAction("Index");

        }

        [HttpPost]
        public async Task<IActionResult> UpdateDataAsync(int id, int index, bool flag)
        {
            var extensionNames = _extensionsService.GetAllExtensions()
                .Select(ex => ex.Name)
                .ToList();
            
            if (index < 0 || index >= extensionNames.Count)
                return BadRequest("Incorrect index of element");

            var bot = await _storage.GetRepository<IBotRepository>().GetAsync(id);
            
            var extensionName = extensionNames[index];
            
            var extension = await _storage.GetRepository<IExtensionRepository>().GetAsync(id, extensionName);

            if (extension != null && extension.IsEnabled == flag) return RedirectToAction("Index");
            
            extension ??= new Entities.Extension()
            {
                Bot = bot,
                Name = extensionName
            };

            extension.IsEnabled = flag;
            
            await _storage.GetRepository<IExtensionRepository>().SaveOrUpdateAsync(extension);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> SetStateAsync(int id, bool flag)
        {
            var bot = await _storage.GetRepository<IBotRepository>().GetAsync(id);
            
            if (bot == null)
                return BadRequest($"Telegram bot with Id: {id} not found");
            
            bot.IsEnabled = flag;
            
            await _storage.GetRepository<IBotRepository>().SaveOrUpdateAsync(bot);
            
            if (flag)
                await _botService.TrySetWebHook(bot);
            
            else
                await _botService.TryRemoveWebHook(bot);
            
            return RedirectToAction("Index");
        }
    }
}