using System.Threading.Tasks;
using BotManager.Abstractions;
using BotManager.Entities;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace BotManager.WebApplication.Controllers
{
    [Route("api/[controller]")]
    public class UpdateController : Controller
    {
        [HttpPost("{token}")]
        public async Task<IActionResult> Post(string token, 
            [FromBody] Update update, 
            [FromServices] IExtensionsService extensions,
            [FromServices] IBotService botService)
        {
            if (!Token.TryParse(token, out var parsedToken)) 
                return BadRequest("Could not determine token");
            
            var client = await botService.GetClientAsync(parsedToken);

            await extensions.ExecuteAsync(client, update);

            return Ok();
        }
    }
}