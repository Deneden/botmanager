﻿using BotManager.Entities;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BotManager.WebApplication.Models
{
    public class IndexModel
    {
        public Dictionary<Bot, List<bool>> BotsWithExtensions { get; } = new Dictionary<Bot, List<bool>>();
        public List<string> ExtensionNames { get; set; } = new List<string>();
        public List<string> ComponentNames { get; set; } = new List<string>();
        public Dictionary<int, List<JObject>> Settings { get; } = new Dictionary<int, List<JObject>>();
    }
}