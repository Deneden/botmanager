﻿using System.Collections.Generic;

namespace BotManager.WebApplication.Models
{
    public class ModuleModel
    {
        public List<(string, bool)> Data { get; set; }
        
        public bool[] Flags { get; set; }
        
        public int BotId { get; set; }
    }
}
