using System;
using ExtCore.Mvc.Infrastructure.Actions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace BotManager.WebApplication.Actions
{
    public class UseEndpointsAction : IUseEndpointsAction
    {
        public void Execute(IEndpointRouteBuilder endpointRouteBuilder, IServiceProvider serviceProvider)
        {
            endpointRouteBuilder.MapControllerRoute(
                name: "Default", 
                pattern: "{controller}/{action}", 
                defaults: new { controller = "Home", action = "Index" });
        }

        public int Priority => 1000;
    }
}