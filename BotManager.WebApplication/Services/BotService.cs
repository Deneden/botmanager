using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BotManager.Abstractions;
using BotManager.Entities;
using ExtCore.Data.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Telegram.Bot;

namespace BotManager.WebApplication.Services
{
    /// <summary>
    /// Bots service
    /// </summary>
    public class BotService : IBotService
    {
        private readonly IOptions<AppConfig> _options;
        private readonly IServiceProvider _serviceProvider;
        private readonly WebProxy _proxy;

        public BotService(IOptions<AppConfig> options, IServiceProvider serviceProvider)
        {
            _options = options ?? throw new ArgumentNullException(nameof(options));
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            _proxy = string.IsNullOrEmpty(options.Value.Socks5Host)
                ? null
                : new WebProxy(options.Value.Socks5Host, options.Value.Socks5Port)
                {
                    Credentials = new NetworkCredential(options.Value.UserName, options.Value.Password)
                };
        }

        /// <inheritdoc/>
        public IList<TelegramBotClient> Clients { get; } = new List<TelegramBotClient>();

        /// <inheritdoc/>
        public TelegramBotClient AddClient(Bot bot)
        {
            var client = _proxy == null
                ? new TelegramBotClient(bot.Token.ToString())
                : new TelegramBotClient(bot.Token.ToString(), _proxy);

            Clients.Add(client);

            return client;
        }

        /// <inheritdoc/>
        public async Task<TelegramBotClient> GetClientAsync(Token token)
        {
            using var scope = _serviceProvider.CreateScope();

            var storage = scope.ServiceProvider.GetRequiredService<IStorage>();

            var bot = await storage.GetRepository<IBotRepository>().GetAsync(token);

            return bot == null
                ? null
                : Clients.Any(x => x.BotId == bot.BotId)
                    ? Clients.First(x => x.BotId == bot.BotId)
                    : AddClient(bot);
        }

        /// <inheritdoc/>
        public async Task<bool> TrySetWebHook(Bot bot)
        {
            var client = new TelegramBotClient(bot.Token.ToString());

            await client.SetWebhookAsync(_options.Value.WebHookUrl + bot.Token);

            var info = await client.GetWebhookInfoAsync();

            return info.Url == _options.Value.WebHookUrl;
        }

        /// <inheritdoc/>
        public async Task<bool> TryRemoveWebHook(Bot bot)
        {
            var client = new TelegramBotClient(bot.Token.ToString());

            await client.DeleteWebhookAsync();

            var info = await client.GetWebhookInfoAsync();

            return string.IsNullOrEmpty(info.Url);
        }
    }
}