using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotManager.Abstractions;
using ExtCore.Data.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotManager.WebApplication.Services
{
    /// <summary>
    /// Extensions service
    /// </summary>
    public class ExtensionsService : IExtensionsService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IStorage _storage;

        public ExtensionsService(IServiceProvider serviceProvider, IStorage storage)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));
        }

        /// <inheritdoc/>
        public async Task ExecuteAsync(TelegramBotClient client, Update update)
        {
            var extensionNames = _storage.GetRepository<IExtensionRepository>()
                .Get(client)
                .Where(x => x.IsEnabled)
                .Select(x => x.Name);

            var services = _serviceProvider.GetServices<IUpdateService>()
                .Where(x => extensionNames.Contains(x.Name));

            foreach (var service in services)
            {
                await service.EchoAsync(client, update);
            }
        }

        /// <inheritdoc/>
        public List<IUpdateService> GetAllExtensions()
        {
            return _serviceProvider.GetServices<IUpdateService>().ToList();
        }
    }
}