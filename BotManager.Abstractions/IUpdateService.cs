using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Update service
    /// </summary>
    public interface IUpdateService
    {
        /// <summary>
        /// Service name
        /// </summary>
        public string Name { get; }
        
        /// <summary>
        /// Component name
        /// </summary>
        public string ComponentName { get; }
        
        /// <summary>
        /// Processing response form bot
        /// </summary>
        /// <param name="client"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        Task EchoAsync(TelegramBotClient client, Update update);
    }
}