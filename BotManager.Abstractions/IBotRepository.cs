using System.Linq;
using System.Threading.Tasks;
using BotManager.Entities;
using ExtCore.Data.Abstractions;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Telegram bots repository
    /// </summary>
    public interface IBotRepository : IRepository
    {
        /// <summary>
        /// Get all telegram bots
        /// </summary>
        /// <returns></returns>
        IQueryable<Bot> GetAll();

        /// <summary>
        /// Get telegram bot by token async
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<Bot> GetAsync(Token token);
        
        /// <summary>
        /// Get telegram bot by Id async
        /// </summary>
        /// <param name="botId"></param>
        /// <returns></returns>
        Task<Bot> GetAsync(int botId);

        /// <summary>
        /// Save bot telegram async
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        Task SaveAsync(Bot bot);
        
        /// <summary>
        /// Save or update telegram bot async
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        Task SaveOrUpdateAsync(Bot bot);
        
        /// <summary>
        /// Remove telegram bot async
        /// </summary>
        /// <param name="botId"></param>
        /// <returns></returns>
        Task RemoveAsync(int botId);
    }
}