using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Extensions service
    /// </summary>
    public interface IExtensionsService
    {
        /// <summary>
        /// Execute service logic
        /// </summary>
        /// <param name="client"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        Task ExecuteAsync(TelegramBotClient client, Update update);
        
        /// <summary>
        /// Get all registered extensions
        /// </summary>
        /// <returns></returns>
        List<IUpdateService> GetAllExtensions();
    }
}