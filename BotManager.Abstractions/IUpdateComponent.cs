﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Update component
    /// </summary>
    public interface IUpdateComponent
    {
        /// <summary>
        /// Invoke update method
        /// </summary>
        /// <param name="id"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        IViewComponentResult Invoke(int id, JObject settings);
    }
}
