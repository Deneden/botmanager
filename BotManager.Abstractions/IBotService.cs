using System.Collections.Generic;
using System.Threading.Tasks;
using BotManager.Entities;
using Telegram.Bot;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Bot service interface
    /// </summary>
    public interface IBotService
    {
        /// <summary>
        /// Get all telegram bot clients
        /// </summary>
        IList<TelegramBotClient> Clients { get; }

        /// <summary>
        /// Add new telegram bot client
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        TelegramBotClient AddClient(Bot bot);

        /// <summary>
        /// Get telegram bot client async by token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        Task<TelegramBotClient> GetClientAsync(Token token);

        /// <summary>
        /// Try set web hooks for telegram bot
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        Task<bool> TrySetWebHook(Bot bot);

        /// <summary>
        /// Try remove web hooks for telegram bot
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        Task<bool> TryRemoveWebHook(Bot bot);
    }
}