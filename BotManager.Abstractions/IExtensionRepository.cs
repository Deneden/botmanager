using System.Collections.Generic;
using System.Threading.Tasks;
using BotManager.Entities;
using ExtCore.Data.Abstractions;
using Telegram.Bot;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Extensions repository
    /// </summary>
    public interface IExtensionRepository : IRepository
    {
        /// <summary>
        /// Get all extensions
        /// </summary>
        /// <returns></returns>
        IEnumerable<Extension> GetAll();

        /// <summary>
        /// Get all extensions for telegram bot client
        /// </summary>
        /// <param name="botClient"></param>
        /// <returns></returns>
        IEnumerable<Extension> Get(TelegramBotClient botClient);

        /// <summary>
        /// Get extension by telegram bot Id and extension name
        /// </summary>
        /// <param name="botId"></param>
        /// <param name="extensionName"></param>
        /// <returns></returns>
        Task<Extension> GetAsync(int botId, string extensionName);

        /// <summary>
        /// Save extension
        /// </summary>
        /// <param name="extension"></param>
        Task SaveAsync(Extension extension);

        /// <summary>
        /// Save or update extension
        /// </summary>
        /// <param name="extension"></param>
        Task SaveOrUpdateAsync(Extension extension);

        /// <summary>
        /// Remove extension
        /// </summary>
        /// <param name="extensionId"></param>
        Task RemoveAsync(int extensionId);
    }
}