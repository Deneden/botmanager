using System.Linq;
using System.Threading.Tasks;
using BotManager.Entities;
using ExtCore.Data.Abstractions;

namespace BotManager.Abstractions
{
    /// <summary>
    /// Tokens repository
    /// </summary>
    public interface ITokenRepository : IRepository
    {
        /// <summary>
        /// Get all tokens
        /// </summary>
        /// <returns></returns>
        IQueryable<Token> GetAll();

        /// <summary>
        /// Get token
        /// </summary>
        /// <param name="botExternalId"></param>
        /// <param name="botToken"></param>
        /// <returns></returns>
        Task<Token> GetAsync(int botExternalId, string botToken);
    }
}