using System;
using System.Linq;
using System.Threading.Tasks;
using BotManager.Abstractions;
using ExtCore.Data.Abstractions;
using Extension.Hello.Models;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Extension.Hello.Services
{
    public class UpdateService : IUpdateService
    {
        private readonly IStorage _storage;

        public UpdateService(IStorage storage)
        {
            _storage = storage ?? throw new ArgumentNullException(nameof(storage));

            Name = GetType().FullName;
            ComponentName = "Update";
        }

        public string Name { get; }
        public string ComponentName { get; }

        public async Task EchoAsync(TelegramBotClient client, Update update)
        {
            var extension = _storage.GetRepository<IExtensionRepository>()
                .Get(client)
                .FirstOrDefault(x => x.Name == Name);
            
            var options = extension?.GetOptions<Options>();
            
            if (update.Type != UpdateType.Message)
                return;

            var message = update.Message;    

            switch (message.Type)
            {
                case MessageType.Text:
                    await client.SendTextMessageAsync(message.Chat.Id, $"{options?.Greeting} {message.From.Username}");
                    break;
            }
        }
    }
}