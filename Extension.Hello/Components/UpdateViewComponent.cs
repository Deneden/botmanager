﻿using BotManager.Abstractions;
using Extension.Hello.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Extension.Hello.Components
{
    public class UpdateViewComponent : ViewComponent, IUpdateComponent
    {
        public IViewComponentResult Invoke(int id, JObject settings)
        {
            var options = settings?.ToObject<Options>();
            return View((id, options?.Greeting));
        }
    }
}
