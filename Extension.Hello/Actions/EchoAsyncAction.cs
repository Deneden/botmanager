using System;
using BotManager.Abstractions;
using ExtCore.Infrastructure.Actions;
using Extension.Hello.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Extension.Hello.Actions
{
    public class EchoAsyncAction : IConfigureServicesAction
    {
        public void Execute(IServiceCollection services, IServiceProvider serviceProvider)
        {
            services.AddScoped<IUpdateService, UpdateService>();
        }

        public int Priority => 1000;
    }
}