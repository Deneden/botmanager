﻿using System.Threading.Tasks;
using BotManager.Abstractions;
using ExtCore.Data.Abstractions;
using Extension.Hello.Models;
using Extension.Hello.Services;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Extension.Hello.Controllers
{
    public class ComponentsController : Controller
    {
        private readonly IStorage _storage;
        private readonly IExtensionRepository _extensionRepository;


        public ComponentsController(IStorage storage)
        {
            _storage = storage;
            _extensionRepository = storage.GetRepository<IExtensionRepository>();
        }

        [HttpPost]
        public async Task<IActionResult> Update(int id, string settings)
        {
            if (id < 0 || string.IsNullOrEmpty(settings))
                return BadRequest("Incorrect data");

            var extension = await _extensionRepository.GetAsync(id, new UpdateService(_storage).Name);

            if (extension == null) return RedirectToAction("Index", "Home");
            
            var options = extension.GetOptions<Options>();
            
            options.Greeting = settings;
            
            extension.Options = JObject.FromObject(options);
            
            await _extensionRepository.SaveOrUpdateAsync(extension);

            return RedirectToAction("Index", "Home");
        }
    }
}