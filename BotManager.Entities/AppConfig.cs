namespace BotManager.Entities
{
    public class AppConfig
    {
        public string Socks5Host { get; set; }

        public int Socks5Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
        
        public string WebHookUrl { get; set; }
    }
}