using ExtCore.Data.Entities.Abstractions;
using Newtonsoft.Json.Linq;

namespace BotManager.Entities
{
    public class Extension : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Bot Bot { get; set; }

        public JObject Options { get; set; }

        public bool IsEnabled { get; set; }

        public T GetOptions<T>() where T : new() =>
            Options != null ? Options.ToObject<T>() : new T();
    }
}