using System.Collections.Generic;
using ExtCore.Data.Entities.Abstractions;

namespace BotManager.Entities
{
    public class Bot : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Token Token { get; set; }

        public bool IsEnabled { get; set; }

        public List<Extension> Extensions { get; set; }

        public int? BotId => Token?.BotExternalId;
    }
}