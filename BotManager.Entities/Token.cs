using ExtCore.Data.Entities.Abstractions;

namespace BotManager.Entities
{
    public class Token : IEntity
    {
        public int Id { get; set; }

        public int BotExternalId { get; set; }

        public string BotToken { get; set; }

        public override string ToString() =>
            $"{BotExternalId.ToString()}:{BotToken}";

        public static bool TryParse(string tokenStr, out Token token)
        {
            token = null;

            var splitToken = tokenStr?.Split(':');

            if (splitToken?.Length != 2)
                return false;
            if (!int.TryParse(splitToken[0], out int tokenId))
                return false;
            if (tokenId < 0)
                return false;
            if (splitToken[1].Length < 10)
                return false;
            
            token = Parse(tokenStr);
            
            return true;
        }

        private static Token Parse(string token) => new Token
        {
            BotExternalId = int.Parse(token.Split(':')[0]),
            BotToken = token.Split(':')[1]
        };
    }
}