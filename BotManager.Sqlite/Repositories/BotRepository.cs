using System.Linq;
using System.Threading.Tasks;
using BotManager.Abstractions;
using BotManager.Entities;
using ExtCore.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace BotManager.Sqlite.Repositories
{
    /// <summary>
    /// Telegram bots repository
    /// </summary>
    public class BotRepository : RepositoryBase<Bot>, IBotRepository
    {
        ///<inheritdoc/>
        public IQueryable<Bot> GetAll() =>
            dbSet.Include(x => x.Token);

        ///<inheritdoc/>
        public async Task<Bot> GetAsync(Token token) =>
            await dbSet.Include(x => x.Token)
                .SingleOrDefaultAsync(x => x.Token.BotToken == token.BotToken
                                           && x.Token.BotExternalId == token.BotExternalId);

        ///<inheritdoc/>
        public async Task<Bot> GetAsync(int botId) =>
            await dbSet.Include(x => x.Token)
                .SingleOrDefaultAsync(x => x.Id == botId);

        ///<inheritdoc/>
        public async Task RemoveAsync(int botId)
        {
            var bot = await dbSet
                .Include(x => x.Extensions)
                .FirstAsync(x => x.Id == botId);

            await using var transaction = await storageContext.Database.BeginTransactionAsync();
            if (bot.Extensions.Any())
            {
                foreach (var extension in bot.Extensions)
                {
                    storageContext.Remove(extension);
                }
            }

            dbSet.Remove(bot);

            await storageContext.SaveChangesAsync();

            await transaction.CommitAsync();
        }

        ///<inheritdoc/>
        public async Task SaveAsync(Bot bot)
        {
            await dbSet.AddAsync(bot);
            await storageContext.SaveChangesAsync();
        }

        ///<inheritdoc/>
        public async Task SaveOrUpdateAsync(Bot bot)
        {
            var foundedBot = await dbSet.FindAsync(bot.Id);
            if (foundedBot == null)
                await dbSet.AddAsync(bot);
            await storageContext.SaveChangesAsync();
        }
    }
}