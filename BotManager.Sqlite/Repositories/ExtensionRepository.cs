using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BotManager.Abstractions;
using ExtCore.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Extension = BotManager.Entities.Extension;

namespace BotManager.Sqlite.Repositories
{
    /// <summary>
    /// Extensions repository
    /// </summary>
    public class ExtensionRepository : RepositoryBase<Extension>, IExtensionRepository
    {
        /// <inheritdoc/>
        public IEnumerable<Extension> GetAll() =>
            dbSet.Include(x => x.Bot).ThenInclude(x => x.Token);

        /// <inheritdoc/>
        public IEnumerable<Extension> Get(TelegramBotClient botClient) =>
            GetAll().Where(x => x.Bot.BotId == botClient.BotId);

            /// <inheritdoc/>
        public async Task SaveAsync(Extension extension)
        {
            await dbSet.AddAsync(extension);
            await storageContext.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task RemoveAsync(int extensionId)
        {
            var extension = await dbSet.FindAsync(extensionId);
            if (extension != null)
            {
                dbSet.Remove(extension);
                await storageContext.SaveChangesAsync();
            }
        }

        /// <inheritdoc/>
        public async Task SaveOrUpdateAsync(Extension extension)
        {
            var foundExtension = await dbSet
                .FirstOrDefaultAsync(ex => ex.Name == extension.Name && ex.Bot.Id == extension.Bot.Id);

            if (foundExtension == null) await dbSet.AddAsync(extension);

            await storageContext.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task<Extension> GetAsync(int botId, string extensionName)
        {
            return await dbSet.Include(i => i.Bot)
                .ThenInclude(b => b.Token)
                .Where(i => i.Bot.Id == botId && i.Name == extensionName)
                .SingleOrDefaultAsync();
        }
    }
}