using System.Linq;
using System.Threading.Tasks;
using BotManager.Abstractions;
using BotManager.Entities;
using ExtCore.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;

namespace BotManager.Sqlite.Repositories
{
    /// <summary>
    /// Token repository
    /// </summary>
    public class TokenRepository : RepositoryBase<Token>, ITokenRepository
    {
        /// <inheritdoc/>
        public IQueryable<Token> GetAll() => dbSet;

        /// <inheritdoc/>
        public async Task<Token> GetAsync(int botExternalId, string botToken) =>
            await dbSet.SingleOrDefaultAsync(t => t.BotExternalId == botExternalId && t.BotToken == botToken);
    }
}