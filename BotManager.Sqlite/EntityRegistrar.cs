using BotManager.Entities;
using ExtCore.Data.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Extension = BotManager.Entities.Extension;

namespace BotManager.Sqlite
{
    public class EntityRegistrar : IEntityRegistrar
    {
        public void RegisterEntities(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bot>(etb =>
                {
                    etb.HasKey(e => e.Id);
                    etb.Property(e => e.Id);
                    etb.HasOne(e => e.Token);
                    etb.ToTable("Bots");
                }
            );

            modelBuilder.Entity<Token>(etb =>
                {
                    etb.HasKey(e => e.Id);
                    etb.Property(e => e.Id);
                    etb.ToTable("Tokens");
                }
            );

            modelBuilder.Entity<Extension>(etb =>
                {
                    etb.HasKey(e => e.Id);
                    etb.Property(e => e.Id);
                    etb.Property(e => e.Options).HasConversion(
                        v => v.ToString(),
                        v => JObject.Parse(v));
                    etb.ToTable("Extensions");
                }
            );
        }
    }
}